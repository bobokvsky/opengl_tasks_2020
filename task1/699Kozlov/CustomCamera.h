//
// Created by bobokvsky on 06.03.2020.
//

#include "common/Camera.hpp"

/**
Камера всегда летит вперед
*/
class CustomFreeCameraMover : public CameraMover
{
public:
    CustomFreeCameraMover(Maze* _maze, int* _currentCamera);

    void handleKey(GLFWwindow* window, int key, int scancode, int action, int mods) override;
    void handleMouseMove(GLFWwindow* window, double xpos, double ypos) override;
    void handleScroll(GLFWwindow* window, double xoffset, double yoffset) override;
    void update(GLFWwindow* window, double dt) override;

protected:
    glm::vec3 _pos;
    glm::quat _rot;

    // Type of camera
    int* _currentCamera;
    Maze* _maze;

    // Положение курсора мыши на предыдущем кадре
    double _oldXPos = 0.0;
    double _oldYPos = 0.0;

    // Константная высота камеры
    double _pos_z;

    // Скорость движения камеры
    float _speed = 20.0f;
};