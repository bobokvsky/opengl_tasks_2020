//
// Created by bobokvsky on 06.03.2020.
//

#include <vector>
#include <iostream>
#include <fstream>

#include "Maze.h"
#include "common/Mesh.hpp"
#include "Plane.cpp"
#include "Parallelepiped.h"

Maze::Maze(std::string maze_path) {
    std::ifstream file;
    file.open(maze_path);

    if (file.fail())
    {
        std::cerr << "Error opening file" << std::endl;
        exit(1);
    }

    std::cout << "Maze initialization..." << std::endl;
    file >> _height >> _width;
    std::cout << "Got height = " << _height << "; width = " << _width << std::endl;
    _maze = std::vector<std::vector<bool>>();
    for (int x = 0; x < _width; x++) {
        auto row = std::vector<bool>();
        for (int y = 0; y < _height; y++) {
            bool value;
            file >> value;
            row.push_back(value);
        }
        _maze.push_back(row);
    }
    file.close();
    std::cout << "Maze ready!" << std::endl;
}

std::vector<MeshPtr> Maze::make_planes(float size) {
    _size = size;
    std::vector<MeshPtr> planes = std::vector<MeshPtr>();

    std::cout << "Making maze planes..." << std::endl;
    for (int x = 0; x < _width; x++) {
        for (int y = 0; y < _height; y++) {

            auto center = glm::vec2(size * x + size / 2., size * y + size / 2.);
            auto left = glm::vec2(size * x, size * y + size / 2.);
            auto bottom = glm::vec2(size * x + size / 2, size * y);
            auto right = glm::vec2(size * x + size, size * y + size / 2.);
            auto top = glm::vec2(size * x + size / 2., size * y + size);

            if (_maze[x][y]) {

//                MeshPtr plane_floor = makePlane(0.5*size, PLANE_XY);
                MeshPtr plane_floor = makeParallelepiped(size, size, 0.1*size);

                auto mat_floor = glm::translate(glm::mat4(1.0f), glm::vec3(center.x, center.y, 0.0f));
                plane_floor->setModelMatrix(mat_floor);
                planes.push_back(plane_floor);

//                MeshPtr plane_ceil = makePlane(0.5*size, PLANE_XY);
                MeshPtr plane_ceil = makeParallelepiped(size, size, 0.1*size);
                auto mat_ceil = glm::translate(glm::mat4(1.0f), glm::vec3(center.x, center.y, size));
                plane_ceil->setModelMatrix(mat_ceil);
                planes.push_back(plane_ceil);

                if (x == 0 or not _maze[x - 1][y]) {
//                    MeshPtr wall = makePlane(0.5*size, PLANE_YZ);
                    MeshPtr wall = makeParallelepiped(0.1*size, size, size);
                    auto mat_wall = glm::translate(glm::mat4(1.0f), glm::vec3(left.x, left.y, 0.5*size));
                    wall->setModelMatrix(mat_wall);
                    planes.push_back(wall);
                }
                if (y == 0 or not _maze[x][y - 1]) {
//                    MeshPtr wall = makePlane(0.5*size, PLANE_XZ);
                    MeshPtr wall = makeParallelepiped(size, 0.1*size, size);
                    auto mat_wall = glm::translate(glm::mat4(1.0f), glm::vec3(bottom.x, bottom.y, 0.5*size));
                    wall->setModelMatrix(mat_wall);
                    planes.push_back(wall);
                }
                if (x == _width - 1 or not _maze[x + 1][y]) {
//                    MeshPtr wall = makePlane(0.5*size, PLANE_YZ);
                    MeshPtr wall = makeParallelepiped(0.1*size, size, size);
                    auto mat_wall = glm::translate(glm::mat4(1.0f), glm::vec3(right.x, right.y, 0.5*size));
                    wall->setModelMatrix(mat_wall);
                    planes.push_back(wall);
                }
                if (y == _height - 1 or not _maze[x][y + 1]) {
//                    MeshPtr wall = makePlane(0.5*size, PLANE_XZ);
                    MeshPtr wall = makeParallelepiped(size, 0.1*size, size);
                    auto mat_wall = glm::translate(glm::mat4(1.0f), glm::vec3(top.x, top.y, 0.5*size));
                    wall->setModelMatrix(mat_wall);
                    planes.push_back(wall);
                }
            }
        }
    }

    std::cout << "Planes are ready!" << std::endl;

    return planes;
}

bool Maze::has_wall(glm::vec3 pos) {
//    float pos_x = pos.x + 2.f;
//    float pos_y = pos.y + 2.f;
//
//    if (pos_x < 0.5*_size or pos_x > _size * _width or pos_y < 0.5*_size or pos_y > _size * _width) {
//        return true;
//    }
//    for (int x = 0; x < _width; x++) {
//        for (int y = 0; y < _height; y++) {
//
//            auto left = glm::vec2(_size * x, _size * y + _size / 2.);
//            auto bottom = glm::vec2(_size * x + _size / 2, _size * y);
//            auto right = glm::vec2(_size * x + _size, _size * y + _size / 2.);
//            auto top = glm::vec2(_size * x + _size / 2., _size * y + _size);
//            if (left.x <= pos_x and pos_x <= right.x and bottom.y <= pos_y and pos_y <= top.y) {
//                return bool(1 - _maze[x][y]);
//            }
//
//        }
//    }
    return false;
}