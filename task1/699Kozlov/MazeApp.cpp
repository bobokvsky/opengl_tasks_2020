#define GLM_ENABLE_EXPERIMENTAL

#include "common/Application.hpp"
#include "common/Mesh.hpp"
#include "common/ShaderProgram.hpp"

#include <iostream>
#include <vector>
#include <experimental/filesystem>

#include "Maze.cpp"
#include "CustomCamera.cpp"
#include "Parallelepiped.cpp"

using namespace std::experimental;

/**
Несколько примеров шейдеров
*/
class SampleApplication : public Application
{

public:
    Maze _maze = Maze("699KozlovData1/maze.txt");
    std::vector<MeshPtr> _planes;
    MeshPtr _bunny;

    ShaderProgramPtr _shader;
    int _currentCamera;

    enum cameraType
    {
        Free,
        Gameplay
    };

    SampleApplication() {
        _currentCamera = cameraType::Free;
        _cameraMover = std::make_shared<CustomFreeCameraMover>(&_maze, &_currentCamera);
    }

    void makeScene() override
    {
        Application::makeScene();
        _planes = _maze.make_planes(10.);

        filesystem::path bunnyPath("699KozlovData1/bunny.obj");
        filesystem::path shaderUBOvertPath("699KozlovData1/PlaneShader.vert");
        filesystem::path shaderUBOfragPath("699KozlovData1/PlaneShader.frag");

        _bunny = loadFromFile(bunnyPath);
        _bunny->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(-5.0f, -5.0f, 0.0f)));

        //=========================================================
        //Инициализация шейдеров
        _shader = std::make_shared<ShaderProgram>(shaderUBOvertPath, shaderUBOfragPath);

    }

    void update() override
    {
        Application::update();
    }

    void updateGUI() override
    {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("MIPT OpenGL Sample", nullptr, ImGuiWindowFlags_AlwaysAutoResize))
        {
            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

            ImGui::RadioButton("Free camera", &_currentCamera, cameraType::Free);
            ImGui::RadioButton("Gameplay camera", &_currentCamera, cameraType::Gameplay);
        }
        ImGui::End();
    }

    void draw() override
    {
        Application::draw();

        //Получаем текущие размеры экрана и выставлям вьюпорт
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Подключаем шейдер
        _shader->use();

        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        //Загружаем на видеокарту матрицы модели мешей и запускаем отрисовку
        for (int i = 0; i < _planes.size(); i++) {
            _shader->setMat4Uniform("modelMatrix", _planes[i]->modelMatrix());
            _planes[i]->draw();
        }
        _shader->setMat4Uniform("modelMatrix", _bunny->modelMatrix());
        _bunny->draw();
    }
};

int main()
{
    std::cout << "Hello Maze!\n";
    SampleApplication app;
    app.start();
    return 0;
}