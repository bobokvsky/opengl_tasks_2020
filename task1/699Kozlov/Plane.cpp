//
// Created by bobokvsky on 05.03.2020.
//

#include "Plane.h"


const int PLANE_XY = 0;
const int PLANE_YZ = 1;
const int PLANE_XZ = 2;


MeshPtr makePlane(float size, int orientation)
{
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec4> colors;

    if (orientation == PLANE_XY) {
        // front 1
        vertices.push_back(glm::vec3(-size, size, 0.0));
        vertices.push_back(glm::vec3(size, -size, 0.0));
        vertices.push_back(glm::vec3(size, size, 0.0));
        // front 2
        vertices.push_back(glm::vec3(-size, size, 0.0));
        vertices.push_back(glm::vec3(-size, -size, 0.0));
        vertices.push_back(glm::vec3(size, -size, 0.0));

        normals.push_back(glm::vec3(0.0, 0.0, 1.0));
        normals.push_back(glm::vec3(0.0, 0.0, 1.0));
        normals.push_back(glm::vec3(0.0, 0.0, 1.0));

        normals.push_back(glm::vec3(0.0, 0.0, 1.0));
        normals.push_back(glm::vec3(0.0, 0.0, 1.0));
        normals.push_back(glm::vec3(0.0, 0.0, 1.0));
    }
    if (orientation == PLANE_YZ) {
        // front 1
        vertices.push_back(glm::vec3(0.0, -size, size));
        vertices.push_back(glm::vec3(0.0, size, -size));
        vertices.push_back(glm::vec3(0.0, size, size));
        // front 2
        vertices.push_back(glm::vec3(0.0, -size, size));
        vertices.push_back(glm::vec3(0.0, -size, -size));
        vertices.push_back(glm::vec3(0.0, size, -size));

        normals.push_back(glm::vec3(1.0, 0.0, 0.0));
        normals.push_back(glm::vec3(1.0, 0.0, 0.0));
        normals.push_back(glm::vec3(1.0, 0.0, 0.0));
        normals.push_back(glm::vec3(1.0, 0.0, 0.0));
        normals.push_back(glm::vec3(1.0, 0.0, 0.0));
        normals.push_back(glm::vec3(1.0, 0.0, 0.0));
    }
    if (orientation == PLANE_XZ) {
        // front 1
        vertices.push_back(glm::vec3(-size, 0., size));
        vertices.push_back(glm::vec3(size, 0., -size));
        vertices.push_back(glm::vec3(size, 0., size));
        // front 2
        vertices.push_back(glm::vec3(-size, 0.0, size));
        vertices.push_back(glm::vec3(-size, 0.0, -size));
        vertices.push_back(glm::vec3(size, 0.0, -size));

        normals.push_back(glm::vec3(0.0, 1.0, 0.0));
        normals.push_back(glm::vec3(0.0, 1.0, 0.0));
        normals.push_back(glm::vec3(0.0, 1.0, 0.0));
        normals.push_back(glm::vec3(0.0, 1.0, 0.0));
        normals.push_back(glm::vec3(0.0, 1.0, 0.0));
        normals.push_back(glm::vec3(0.0, 1.0, 0.0));
    }
    //----------------------------------------

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, colors.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    return mesh;
}
