//
// Created by bobokvsky on 06.03.2020.
//

#ifndef OPENGL_TASKS_2020_MAZE_H
#define OPENGL_TASKS_2020_MAZE_H

#include <vector>
#include <string>

#include <glm/glm.hpp>
#include "common/Mesh.hpp"


class Maze {
    std::vector<std::vector<bool>> _maze;
    int _height;
    int _width;
    float _size;

public:
    Maze(std::string maze_path);
    std::vector<MeshPtr> make_planes(float size);
    bool has_wall(glm::vec3 pos);
};


#endif //OPENGL_T;ASKS_2020_MAZE_H
