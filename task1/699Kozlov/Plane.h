//
// Created by bobokvsky on 05.03.2020.
//

#include "common/Mesh.hpp"

#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <iostream>
#include <vector>

/**
Создает модель плоской поверхности
*/
MeshPtr makePlane(float size, int orientation);